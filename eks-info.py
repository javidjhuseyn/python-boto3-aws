import boto3 as bt

eks_client_london = bt.client('eks', region_name='eu-west-2')

clusters = eks_client_london.list_clusters().get('clusters')

for cluster in clusters:
    response = eks_client_london.describe_cluster(
        name=cluster
    )

    cluster_info = response.get('cluster')
    cluster_status = cluster_info.get('status')
    cluster_endpoint = cluster_info.get('endpoint')
    cluster_version = cluster_info.get('version')
    
    print(f"Cluster - {cluster} status is: {cluster_status}")
    print(f"Cluster endpoint is: {cluster_endpoint}")
    print(f"Cluster version is: {cluster_version}")
