import boto3 as bt
import schedule as sch

ec2_client = bt.client('ec2', region_name='eu-west-2')
ec2_resource = bt.resource('ec2', region_name='eu-west-2')

all_instances = ec2_client.describe_instances()
instance_status = ec2_client.describe_instance_status()

# print(all_instances)
# print(instance_status)

reservations = all_instances.get('Reservations')

# get instance id and state


def check_ins_status():
    for gen_data in reservations:
        instance_data = gen_data.get('Instances')
        # print(data)
        # print(instance_data)
        for spec_data in instance_data:
            # print(spec_data.get('State'))
            instance_id = spec_data.get('InstanceId')
            state = spec_data.get('State')
            state_name = state.get('Name')
            print(f"State of EC2 instance -> {instance_id} is: {state_name}")


# sch.every(5).seconds.do(check_ins_status)
#
# while True:
#     sch.run_pending()


check_ins_status()

# get instance status - both InstanceStatus and SystemStatus

statuses = instance_status.get('InstanceStatuses')
for data in statuses:
    ins_id = data.get('InstanceId')
    ins_sts = data.get('InstanceStatus')
    sys_sts = data.get('SystemStatus')

    # print(ins_sts)
    # print(sys_sts)

    for dt in ins_sts.get('Details'):
        sts = dt.get('Status')
        print(f"Health Check - InstanceStatus of EC2 instance <-- {ins_id} --> is: {sts} and is {ins_sts.get('Status')}")
    for dt in sys_sts.get('Details'):
        sts = dt.get('Status')
        print(f"Health Check - SystemStatus of EC2 instance <-- {ins_id} --> is {sts} and is {sys_sts.get('Status')}")
