import boto3 as bt

ec2_client = bt.client('ec2', region_name="eu-west-2")
ec2_resource = bt.resource('ec2')

new_vpc = ec2_resource.create_vpc(CidrBlock='10.0.0.0/16')
new_vpc.create_tags(
    Tags=[
        {
            'Key': 'Name',
            'Value': 'test-vpc'
        }
    ]
)
vpcs_in_region = ec2_client.describe_vpcs()

num = 1
for vpc in vpcs_in_region.get('Vpcs'):
    print(f"VPC ID - {num} is: {vpc.get('VpcId')}")
    num += 1
